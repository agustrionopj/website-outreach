---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Modul Matahari"
subtitle: ""
summary: ""
authors: ["admin"]
tags: ["matahari", "bintang"]
categories: ["materi teks astronomi"]
date: 2021-01-05T12:00:20+07:00
lastmod: 2021-01-05T12:00:20+07:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat aliquet velit, vitae rhoncus purus imperdiet ut. Maecenas placerat eget lorem sit amet dapibus. Nulla et mi sem. Donec a pretium tellus. Praesent eu eros sit amet massa euismod interdum eu ac erat. Vivamus neque est, ultricies at lectus ac, rhoncus auctor quam. Praesent venenatis enim ac sapien mattis iaculis. Etiam eleifend orci id rutrum pulvinar. Aenean mauris diam, tristique non sapien a, sollicitudin sollicitudin massa. Duis quis rutrum felis. Curabitur nec bibendum ipsum, sed elementum quam.

Sed interdum sapien non metus interdum, et feugiat dolor faucibus. Vestibulum pharetra dui lacus. Suspendisse placerat non ante at accumsan. Nulla semper at justo ut aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse faucibus malesuada mauris sed commodo. Mauris quis ullamcorper augue. Praesent pellentesque blandit bibendum. Pellentesque semper metus in pretium iaculis.

Donec est augue, tincidunt sit amet magna eget, rhoncus euismod augue. Maecenas eget lorem ipsum. Vestibulum tempor enim nibh, pretium molestie ante luctus a. Sed odio sapien, dignissim rhoncus felis sit amet, malesuada consequat nunc. Phasellus sit amet diam nunc. In sollicitudin sem ante, nec consequat lacus interdum non. Nullam tempus nisl sed turpis dapibus vehicula vitae non augue. Nulla viverra et lorem vel rutrum. Quisque congue ipsum at libero hendrerit vulputate eu vitae urna. Mauris ac fringilla mauris, vel fermentum justo. Integer commodo sem ac euismod pulvinar. Suspendisse nec lorem magna. Suspendisse dignissim porttitor est. Aenean consequat nulla nibh, quis maximus lorem rutrum quis.

Fusce at gravida magna. Praesent est turpis, hendrerit sed sapien eu, pretium ornare augue. Ut euismod eu velit non scelerisque. Fusce at interdum lorem, ac pharetra risus. Etiam semper lectus eget quam luctus, at consectetur tellus eleifend. Ut eget dolor neque. Donec quam leo, bibendum et est sit amet, sodales pretium risus. Morbi lobortis enim at imperdiet vestibulum. Nunc sagittis mauris blandit ante maximus, a condimentum elit dignissim. Vestibulum in orci vitae lacus tristique scelerisque non nec metus. In pulvinar, ipsum sit amet rutrum fringilla, elit urna ornare leo, id venenatis ipsum lorem ut diam. Cras id consectetur felis. Integer vestibulum, leo non ornare tincidunt, magna tortor pretium nulla, vel fermentum elit magna vel massa. Phasellus sodales libero eget nibh dictum, vitae sodales nibh scelerisque. Praesent in turpis ante. Aliquam ligula orci, varius sit amet est ac, euismod elementum erat.

Sed eleifend nisi pulvinar ligula fringilla, non tincidunt ipsum fermentum. Maecenas mollis, nulla vitae mollis tincidunt, est ante euismod ante, sed ullamcorper est urna sed ipsum. Sed fringilla risus et massa scelerisque, in efficitur lacus dapibus. Maecenas maximus porttitor quam, eget euismod enim finibus eget. Integer pulvinar ullamcorper leo, a aliquam ex faucibus rhoncus. Ut at rutrum leo, sed efficitur velit. Fusce quam ligula, viverra et nibh non, faucibus vehicula ligula. Aliquam convallis placerat justo, vitae varius dolor imperdiet nec. Nam dignissim eget nisi quis ultricies.